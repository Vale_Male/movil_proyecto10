import 'package:comandas/CuentaUno.dart';
import 'package:comandas/comandasRegistro.dart';
import 'package:comandas/cuentaDos.dart';
import 'package:comandas/reservacionRegistro.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:comandas/comandasDosRegistro.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import 'login.dart';

//import 'package:flutter_application_introduction/ejemplo5.dart';

class cuentaCosultaDos  extends StatefulWidget {
  @override
  _cuentaState createState() => _cuentaState();
}
//Traer comandas
  final String urlComandasConsultas = "https://apis.pegasoirapuato.com/public/api/v1/comandasConsultasControllerEmple/1";
  List<Comandas> comandasListaConsulta = [];

  //Creacion de color verde-agua
  class HexColor extends Color {
    
    static int _getColorFromHex(String hexColor) {
      hexColor = hexColor.toUpperCase().replaceAll("#", "");
      if (hexColor.length == 6) {
        hexColor = "FF" + hexColor;
      }
      return int.parse(hexColor, radix: 16);
    }

    HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
  }
  Color color = HexColor("#86d9ab");
enum SingingCharacter { oro, plata }
int id_empleado=0;


class _cuentaState extends State<cuentaCosultaDos> {

 Future<String> traerComandas() async {
    var res = await http
        .get(Uri.parse(urlComandasConsultas), headers: {"Accept": "application/json"});
    var resBody1 = json.decode(res.body);
    var resBody = resBody1['data'];
    print(resBody);
    List<Comandas> comandasLista = [];
    // String fecha,String hora,int num_personas,int id) {

  print('id_empleado');
  print(id_empleado);
   for(var i = 0; i < resBody.length; i++){

      Comandas reser = new Comandas();
              print(resBody[i]['id'].toString());
              print(resBody[i]['precio']);
              print(resBody[i]['empleado_id']);

            reser.id =resBody[i]['id'];
            reser.precio =resBody[i]['precio'].toString();
            reser.empleado =resBody[i]['empleado_id'].toString();
            print(id_empleado==int.parse(resBody[i]['empleado_id']));
            print(resBody[i]['empleado_id']);
            if(id_empleado==int.parse(resBody[i]['empleado_id'])){
              comandasLista.add(reser);
            }

    }
  print('en la lista');

   for(var i = 0; i < comandasLista.length; i++){
       print(comandasLista[i].id.toString());
              print(comandasLista[i].precio.toString());
              print(comandasLista[i].empleado.toString());
   }
    setState(() {
      comandasListaConsulta = comandasLista;
    });
    return "Sucess";
  }

@override
  void initState() {
    super.initState();
    this.traerComandas();
    cantidad=0.0;
    traer_id();
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
  statusBarColor: Colors.transparent,
));
    String rutaImagen="assets/img/logo.png";
    return  Scaffold(
      backgroundColor: Colors.white,

     body: Center (
       child:SingleChildScrollView(
       
        child: Column(
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.only(
                   top: 25, bottom: 50),
                  child: Center(
                child: Text(
                  'Comandas en cocina',
                  textDirection: TextDirection.ltr,
                  style: TextStyle(
                    fontSize: 32,
                    color: color,
                  ),
                    
              ),
              ),
            ),
            SizedBox(
            child: Card(
              child: Column(
                children: [                
                   Container(
                child:  GridView.builder(
                  physics: ScrollPhysics(),
                  shrinkWrap: true,
                    itemCount: comandasListaConsulta.length,
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      childAspectRatio: 0.5,
                    ),
                    itemBuilder: (context, index) {;
                      return Container(
                        
                        width:500,
                        height:600,
                        margin: EdgeInsets.all(5),
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0x000005cc),
                                  blurRadius: 30,
                                  offset: Offset(10, 10))
                            ]),
                        child: Column(
                          
                          children: <Widget>[
                            Text(
                              'Número de comanda: '+comandasListaConsulta[index].id.toString(),
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 20),
                              child: Text(
                                r"$"+ comandasListaConsulta[index].precio.toString(),
                                style: TextStyle(fontSize: 16),
                              ),
                            ),
                               
                            Row(
                              children:[
                                new SizedBox(
                                  width: 0.0,
                                  height: 50.0,
                                ),
                                new SizedBox(
                                  width: 100.0,
                                  height: 20.0,
                ),
                                new SizedBox(
                                  width: 10.0,
                                  height: 50.0,
                                ),
                              ]

                            )
                         
                            ],
                        ),
                      );
                    }),
              ),


                ],
              ),
            ),
            
          ),
           
          ],
        ),
      
      )
      ),
      appBar: AppBar(
       backgroundColor: color,
          title: Text('Riquisimo!'),
          actions: <Widget>[
            IconButton(
              //Cuenta Uno
              icon: new Icon(Icons.account_balance_wallet_sharp),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return cuentaCosultaDos(      
                  );
                }));
              },
            ),
            IconButton(
              //Cuenta
              
              icon: new Icon(Icons.note),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return cuenta(      
                  );
                }));
              },
            ),
            //Comandas
             IconButton(
              icon: new Icon(Icons.book),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return comandas(      
                  );
                }));
              },
            ),
            //Reservacion
             IconButton(
              icon: new Icon(Icons.menu_book_sharp),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return reservacion(      
                  );
                }));
              },
            ),
             IconButton(
              icon: new Icon(Icons.logout),
              onPressed: () {
               cerrar_sesion();
              },
            ),
          ],
        )
      
    );

  }
  Future<void> cerrar_sesion() async {
    //prefs.remove("counter");
    SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.remove('id_usuario');
      await prefs.remove('nombre');
      await prefs.remove('contrasenia');
      await prefs.remove('rol');
      await prefs.remove('id_empleado');
      Navigator.push(
          context, MaterialPageRoute(builder: (_) => login())
      );
  }

  void traer_id() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      id_empleado = (prefs.getInt('id_empleado'))!;
    });
  }
}
