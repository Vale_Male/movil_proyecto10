import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:comandas/reservacionRegistro.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

//import 'package:flutter_application_introduction/ejemplo5.dart';


class login  extends StatefulWidget {
  @override
  _loginState createState() => _loginState();
}
enum SingingCharacter { oro, plata }
class HexColor extends Color {
  
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
Color color1 = HexColor("#86d9ab");
class _loginState extends State<login> {
    final usuario = TextEditingController();
    final contrasenia = TextEditingController();

  
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
  statusBarColor: Colors.transparent,
));
    String rutaImagen="assets/img/logo.png";
    final card= Container(
      height:160 ,
      width:60,
      margin:EdgeInsets.only(),
      decoration: BoxDecoration(
        image: DecorationImage(fit:BoxFit.cover, image:AssetImage(rutaImagen)),
       
      ),
    );
    return  Scaffold(
      
       backgroundColor: Colors.white,
     body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 60.0),
              child: Center(
                child: Container(
                    width: 250,
                    height: 200,
                    child: Image.asset(rutaImagen)),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                controller: usuario,
                decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    border: OutlineInputBorder(),
                    hintStyle: TextStyle(color: Colors.black,),
                    labelText: 'Usuario',
                    labelStyle: TextStyle(color: color1,),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          color: color1,
                          width: 3
                      )
                  ),),
                    
              ),
            ),
            
            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
              controller: contrasenia,

                obscureText: true,
                decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    border: OutlineInputBorder(),
                    hintStyle: TextStyle(color: Colors.black,),
                    labelText: 'Contraseña',
                    labelStyle: TextStyle(color: color1,),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          color: color1,
                          width: 3
                      )
                  ),
                  ),
              ),
            ),
            SizedBox(
              height: 70
            ),
            Container(
              
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                  color: color1, borderRadius: BorderRadius.circular(20)),
              child: FlatButton(
                onPressed: () {

                  login_post(usuario.text,contrasenia.text);
                     },
                child: Text(
                  'Login',
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
            SizedBox(
              height: 130,
            ),
          ],
        ),
      ),

    );
  }

  var url = "https://apis.pegasoirapuato.com/public/api/v1/login";

  void login_post(String usuario_in, String password) async {
    print("Entro a login psot");
    try{
      final response = await post(Uri.parse(url), body:{
        
        "usuario":usuario_in,
        "contrasenia": password
      });
      print("Entro a login psot4");
      print(response.statusCode);
      
      if(response.statusCode==200){
        var body=response.body;
      var usuarios=jsonDecode(body);
      print("Usuario en prefer");
      print(usuarios);
      var usuario_status=usuarios['usuario'];
      var role=usuario_status['rol'];
        if(role=="Empleado"){
          Usuario usu=new Usuario();

          usu.nombre = usuario_status['nombre'].toString();
          print('Entro 1');
          usu.id = usuario_status['id'];
          usu.contrasenia = usuario_status['contrasenia'];
          usu.rol = usuario_status['rol'].toString();
          usu.idEmpleado = int.parse(usuario_status['empleado_id']);
          _loeguear(usu);
          
        }else{
          _showAlertRole(this.context,);

        }
      }else{
        _showAlertError(this.context,);
      }
    }catch(e){
      print(e);
    }
}

_loeguear(Usuario usu) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  print(usu.nombre);
  await prefs.setInt('id_usuario', usu.id);
  await prefs.setString('nombre', usu.nombre);
  await prefs.setString('contrasenia', usu.contrasenia);
  await prefs.setString('rol', usu.rol);
  await prefs.setInt('id_empleado', usu.idEmpleado);
  Navigator.push(
      context, MaterialPageRoute(builder: (_) => reservacion())
  );
}
  void _showAlertError(BuildContext context) {
      showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('Error! '),
          content: const Text('Proporciona otro usuario u otra contraseña'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return login(      
                  );})),
              child: const Text('OK'),
            ),
          ],
        ));
    }
    void _showAlertRole(BuildContext context) {
      showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('Error! '),
          content: const Text('No tienes acceso a esta aplicación'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return login(      
                  );})),
              child: const Text('OK'),
            ),
          ],
        ));
    }
}
class Usuario{
  late int id;
  late String nombre;
  late String contrasenia;
  late String rol;
  late int idEmpleado;
}
