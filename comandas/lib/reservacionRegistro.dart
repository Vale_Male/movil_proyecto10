import 'dart:convert';

import 'package:comandas/CuentaConsultasDos.dart';
import 'package:comandas/CuentaUno.dart';
import 'package:comandas/reservacionRegistroSiguiente.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:comandas/comandasRegistro.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login.dart';

  class reservacion  extends StatefulWidget {
    @override
    _reservacionState createState() => _reservacionState();
  }
  //Creacion de color verde-agua
  class HexColor extends Color {
    
    static int _getColorFromHex(String hexColor) {
      hexColor = hexColor.toUpperCase().replaceAll("#", "");
      if (hexColor.length == 6) {
        hexColor = "FF" + hexColor;
      }
      return int.parse(hexColor, radix: 16);
    }

    HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
  }
  Color color1 = HexColor("#86d9ab");

  //Creacion de selects y consultas para selects
  List data = []; 
  List dataState = []; 
  String _mySelection="0";
  List dataHo = []; 
  List dataStateHo = []; 
  String _mySelectionHo="0";
  int traer=0;


class _reservacionState extends State<reservacion> {

final String url = "https://apis.pegasoirapuato.com/public/api/v1/mesas";
final String urlDos = "https://apis.pegasoirapuato.com/public/api/v1/horarios?";

//Creacion de datapicker
var current_selected_data;
String select_fecha="";

void callDataPicker() async {
  var selectData = await getDataPickerWidget();
  setState((){
    current_selected_data = selectData;
    String formattedDate = DateFormat('yyyy-MM-dd – kk:mm').format(current_selected_data);
    select_fecha=formattedDate;
  });
}
 Future ? getDataPickerWidget() {
   return showDatePicker(context: context, initialDate: DateTime.now(), 
   firstDate: DateTime(2021), lastDate: DateTime(2022),
  );
 }

//Traer datos selectUno
 Future<String> getSWData() async {
    var res = await http
        .get(Uri.parse(url), headers: {"Accept": "application/json"});
    var resBody1 = json.decode(res.body);
    var resBody = resBody1['data'];

   var resBodys=[];
   var resBodysState=[];
   _mySelection=resBody[0]['numero']+' cant:'+resBody[0]['capacidad'];
   for(var i = 0; i < resBody.length; i++){
      resBodys.add(resBody[i]['numero']+' cant:'+resBody[i]['capacidad']);
      resBodysState.add(resBody[i]);
    }
    setState(() {
      data = resBodys;
      dataState = resBodysState;

    });
    return "Sucess";
  }
 
//Traer datos selectDos
 Future<String> getSWDataSe() async {
   int id_mesas=0;
   print("MESAS: ");
   print(_mySelection);
      for(var i = 0; i < dataState.length; i++){
        
        if(dataState[i]['numero']+' cant:'+dataState[i]['capacidad']==_mySelection){
             id_mesas=dataState[i]['id'];
        }else{
        }
      }
      String str=id_mesas.toString();
      //String str=id_mesas;
   var res = await http
        .get(Uri.parse(urlDos+'fecha='+select_fecha+'&id_mesa='+str), headers: {"Accept": "application/json"});
    var resBody1 = json.decode(res.body);
    var resBody = resBody1['data'];
   var resBodys=[];
   var resBodysState=[];
   dataHo.clear();
   dataStateHo.clear();
   print(resBody);
   for(var i = 0; i < resBody.length; i++){
      resBodys.add(resBody[i]['horario_inicio']+' a '+resBody[i]['horario_fin']);
      resBodysState.add(resBody[i]);
    }
    setState(() {
      dataHo = resBodys;
      dataStateHo = resBodysState;
      _mySelectionHo=resBody[0]['horario_inicio'].toString()+' a '+resBody[0]['horario_fin'].toString();
    });
        return "Sucess";

  }  
void traerDatos() {
  getSWDataSe();
}

late DateTime initialDate;

 @override
  void initState() {
    super.initState();
    this.getSWData();
     DateTime initial = DateTime.now();
    initialDate =
        DateTime(initial.year, initial.month, initial.day, initial.hour, 0);
  }
  @override
  Widget build(BuildContext context) {
    //Traer los datos de select dos cuando se mueva select uno
    if(traer > 0){
        traerDatos();
        traer=-1;
    }

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      
      statusBarColor: Colors.transparent,
    ));
    return  Scaffold(
      
      backgroundColor: Colors.white,

     body: Center (child:SingleChildScrollView(
       
        child: Column(
          children: <Widget>[
             
            Padding(
                padding: const EdgeInsets.only(
                   top: 25, bottom: 50),
                  child: Center(
                child: Text(
                  'Reservación',
                  //textDirection: TextDirection.ltr,
                  style: TextStyle(
                    fontSize: 32,
                    color: color1,
                  ),
                    
              ),
              ),
            ),
            SizedBox(
            height: 600,
            child: Card(
              child: Column(
                children: [
               ListTile(
                         subtitle: Text(
                        'Fecha',
                        style: TextStyle(
                          fontSize: 20,
                          color: color1,
                        ),
                      ),
                      
                    leading: Icon(
                      Icons.support,
                      color: color1,
                    ),
                  ),
                  FlatButton(
                onPressed: callDataPicker,
                child:Text("Selecionar Fecha")
                
              ),
                  ListTile(
                         subtitle: Text(
                        'Número de mesa',
                        style: TextStyle(
                          fontSize: 20,
                          color: color1,
                        ),
                      ),
                      
                    leading: Icon(
                      Icons.support,
                      color: color1,
                    ),
                  ),
                   Container(
                    width: 300.0,
                    padding: EdgeInsets.all(5.0),
                    child: new Row(
                      children:[
                            DropdownButton<String>(
                                items: data.map((Object){
                                  return DropdownMenuItem<String>(
                                    value:Object,
                                    child:Text(Object),
                                  ); 
                                }).toList(),
                                onChanged: (String ? value){
                                  setState(() {
                                    _mySelection = value!;
                                    traer=1;
                                  });
                                },
                                value:_mySelection,
                            ),
                      SizedBox(height: 20,width:150),
                      ]
           )
          ),
          Divider(),
                  ListTile(
                         subtitle: Text(
                        'Horarios',
                        style: TextStyle(
                          fontSize: 20,
                          color: color1,
                        ),
                      ),
                      
                    leading: Icon(
                      Icons.support,
                      color: color1,
                    ),
                  ),
                   Container(
                    width: 400.0,
                    padding: EdgeInsets.all(5.0),
                    child: new Row(
                      children:[
                            DropdownButton<String>(
                                items: dataHo.map((Object){
                                  return DropdownMenuItem<String>(
                                    value:Object,
                                    child:Text(Object),
                                  ); 
                                }).toList(),
                                onChanged: (String ? value){
                                  setState(() {
                                    _mySelectionHo = value!;
                                      // ignore: unused_label
                                  });
                                },
                                value:_mySelectionHo,
                            ),
                      SizedBox(height: 20,width:150),
                      ]
           )
           
           
          ),
                  
              Container(
              decoration: BoxDecoration(
              color: color1, borderRadius: BorderRadius.circular(20)),
              child: FlatButton(
                onPressed: () {
                  int id_mesas=0;
                  for(var i = 0; i < dataState.length; i++){
                    if(dataState[i]['numero']+' cant:'+dataState[i]['capacidad']==_mySelection){
                        id_mesas=dataState[i]['id'];
                    }else{
                    }
                  }
                  var str = _mySelectionHo;
                  var parts = str.split(' ');
                  var prefix = parts[0].trim();                 // prefix: "date"
                  int id_horario=0;
                  for(var i = 0; i < dataStateHo.length; i++){
                    if(dataStateHo[i]['horario_inicio']==prefix){
                        id_horario=dataStateHo[i]['id'];
                    }else{
                    }
                  }
                  Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return reservacionSigu(      
                            select_fecha,id_mesas,id_horario //nomCompleto, signo, rutaImagen
                            );
                      }));/*
                  Navigator.push(
                      context, MaterialPageRoute(builder: (_) => comandas()));*/
                },
                child: Text(
                  'Siguiente',
                  style: TextStyle(color: Colors.black, fontSize: 12),
                ),
              ),
            ),



                ],
              ),
            ),
          ),
           
          ],
        ),
      
      )
      ),
     appBar: AppBar(
       backgroundColor: color1,
          title: Text('Riquisimo!'),
          actions: <Widget>[
            IconButton(
              //Cuenta Uno
              icon: new Icon(Icons.account_balance_wallet_sharp),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return cuentaCosultaDos(      
                  );
                }));
              },
            ),
            IconButton(
              //Cuenta
              icon: new Icon(Icons.note),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return cuenta(      
                  );
                }));
              },
            ),
            //Comandas
             IconButton(
              icon: new Icon(Icons.book),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return comandas(      
                  );
                }));
              },
            ),
            //Reservacion
             IconButton(
              icon: new Icon(Icons.menu_book_sharp),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return reservacion(      
                  );
                }));
              },
            ),
             IconButton(
              icon: new Icon(Icons.logout),
              onPressed: () {
               cerrar_sesion();
              },
            ),
          ],
        )
    );
  }

  Future<void> cerrar_sesion() async {
    //prefs.remove("counter");
    SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.remove('id_usuario');
      await prefs.remove('nombre');
      await prefs.remove('contrasenia');
      await prefs.remove('rol');
      await prefs.remove('id_empleado');
      Navigator.push(
          context, MaterialPageRoute(builder: (_) => login())
      );
  } 
}


