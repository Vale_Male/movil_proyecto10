import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:comandas/reservacionRegistro.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login.dart';

//import 'package:flutter_application_introduction/ejemplo5.dart';


class decidir  extends StatefulWidget {
  @override
  _decidirState createState() => _decidirState();
}

class _decidirState extends State<decidir> {
    final usuario = TextEditingController();
    final contrasenia = TextEditingController();
    @override
      void initState() {
        super.initState();
        getUserStatus().then((userStatus) {
          print('userStatus en clase');
          print(userStatus.toString());
          if (userStatus) {
            Navigator.of(context)
                .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
              return login();
            }));
          } else {
            Navigator.of(context)
                .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
              return reservacion();
            }));
          }
        });
      }  
      @override
      Widget build(BuildContext context) {
        return Container(
            child: Center(
          child: CircularProgressIndicator(),
        ));
      }
      Future<bool> getUserStatus() async {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        String userStatus = prefs.getString('nombre').toString();
        print(userStatus);
        bool user;
        if(userStatus==null){
            user = false;
        }else{
            user = true;
        }
        return user;
      }
}




