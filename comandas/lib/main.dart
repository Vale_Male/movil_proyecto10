import 'package:comandas/CuentaUno.dart';
import 'package:comandas/reservacionRegistro.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'decidir.dart';
import 'login.dart';

Future<void> main() async {
   //SystemChrome.setEnabledSystemUIOverlays([]);  

runApp(MyApp());
}


class MyApp extends StatelessWidget{


@override
  Widget build(BuildContext context){
    //SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    return MaterialApp(
      //quita etiqueta roja
      debugShowCheckedModeBanner: false,
      title:"Riquísimo",
      theme: ThemeData(primarySwatch: Colors.green),
      home: new decidir()
     );
  }
 }
