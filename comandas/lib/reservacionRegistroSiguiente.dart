import 'dart:convert';
import 'package:comandas/CuentaConsultasDos.dart';
import 'package:comandas/CuentaUno.dart';
import 'package:comandas/reservacionRegistro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:comandas/comandasRegistro.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'login.dart';


enum SingingCharacter { oro, plata }
class HexColor extends Color {
  
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

Color color1 = HexColor("#86d9ab");
List<dynamic> _ColoresUno = ['15', '15', '15'];
  List data = []; 
  List dataState = []; 
  String _mySelection="0";

class reservacionSigu  extends StatefulWidget {
  final String select_fecha;
  final int id_mesa;
  final int id_horario;

  reservacionSigu(this.select_fecha, this.id_mesa, this.id_horario);
//                            select_fecha,strs,id_horario.toString() //nomCompleto, signo, rutaImagen

  @override
  _reservacionState createState() => _reservacionState(this.select_fecha, this.id_mesa, this.id_horario);
}
int id_empleado=0;

class _reservacionState extends State<reservacionSigu> {
  final cliente = TextEditingController();
  final observacion = TextEditingController();
  final numero_personas = TextEditingController();
    
  final String select_fecha;
  final int id_mesa;
  final int id_horario;
  
  //this.select_fecha;
  var current_selected_data;
 final String url = "https://apis.pegasoirapuato.com/public/api/v1/mesas";

  _reservacionState( this.select_fecha, this.id_mesa, this.id_horario);


void callDataPicker() async {
  var selectData = await getDataPickerWidget();
  setState((){
    current_selected_data = selectData;
    print(current_selected_data);
  });
}
 Future ? getDataPickerWidget() {
   return showDatePicker(context: context, initialDate: DateTime.now(), 
   firstDate: DateTime(2021), lastDate: DateTime(2022),
  );
 }

//edited line
 Future<String> getSWData() async {
    var res = await http
        .get(Uri.parse(url), headers: {"Accept": "application/json"});
    var resBody1 = json.decode(res.body);
    var resBody = resBody1['data'];

   var resBodys=[];
   var resBodysState=[];
   _mySelection=resBody[0]['numero'];
   for(var i = 0; i < resBody.length; i++){
      resBodys.add(resBody[i]['numero']);
      resBodysState.add(resBody[i]);
    }


    setState(() {
      data = resBodys;
      dataState = resBodysState;

    });
    return "Sucess";
  }
late DateTime initialDate;

 @override
  void initState() {
    super.initState();
    this.getSWData();
    this.traer_id();
    DateTime initial = DateTime.now();
    initialDate =
        DateTime(initial.year, initial.month, initial.day, initial.hour, 0);
            print('initialDate');
            print(initialDate);

  }
  @override
  Widget build(BuildContext context) {
     ;
      print('Select fecha');
      print(this.select_fecha);
      print('id_mesa');
      print(this.id_mesa);
      print('id_horario');
      print(this.id_horario);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      
      statusBarColor: Colors.transparent,
    ));
    return  Scaffold(
      
      backgroundColor: Colors.white,

     body: Center (child:SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.only(top: 25, bottom: 50),
                child: Center(
                  child: Text(
                    'Reservación',
                    textDirection: TextDirection.ltr,
                    style: TextStyle(
                      fontSize: 32,
                      color: color1,
                    ),
                  ),
                ),
            ),
            SizedBox(
              height: 600,
              child: Card(
                child: Column(
                  children: [
                  
                    ListTile(
                          subtitle: TextField(
                          controller: cliente,

                          decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              hintStyle: TextStyle(color: Colors.black,),
                              labelText: 'Cliente',
                              labelStyle: TextStyle(color: color1,),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: color1,
                                    width: 3
                                )
                            ),
                            ),
                        ),
                      leading: Icon(
                        Icons.supervisor_account,
                        color: color1,
                      ),
                    ),
                    Divider(),
                    ListTile(
                          subtitle: TextField(
                          controller: observacion,

                          style: TextStyle(
                            height: 2.5
                          ),    decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              hintStyle: TextStyle(color: Colors.black,),
                              labelText: 'Observaciónes',
                              labelStyle: TextStyle(color: color1,),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: color1,
                                    width: 3
                                )
                            ),
                            ),
                        ),
                      leading: Icon(
                        Icons.book,
                        color: color1,
                      ),
                    ),
                    Divider(),
                      ListTile(
                          subtitle: TextField(
                          controller: numero_personas,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              hintStyle: TextStyle(color: Colors.black,),
                              labelText: 'Personas',
                              labelStyle: TextStyle(color: color1,),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: color1,
                                    width: 3
                                )
                            ),
                            ),
                        ),

                    leading: Icon(
                        Icons.supervised_user_circle,
                        color: color1,
                      ), 
                    ),
                    Divider(),
                    Container(
                    height: 40,
                    width: 90,
                    decoration: BoxDecoration(
                        color: color1, borderRadius: BorderRadius.circular(20)),
                    child: FlatButton(
                      onPressed: () {
                        //  String cliente, String observacion, String fecha, int id_horario, String num_personas
                      reservacion_post(this.id_mesa,id_empleado,cliente.text,observacion.text,this.select_fecha,this.id_horario,numero_personas.text);
                      },
                      child: Text(
                        'Agregar',
                        style: TextStyle(color: Colors.black, fontSize: 15),
                      ),
                    ),
                  ),
                  ],
                ),
              ),
            ),
          ],
        ),
      )),
          appBar: AppBar(
       backgroundColor: color1,
          title: Text('Riquisimo!'),
          actions: <Widget>[
            IconButton(
              //Cuenta Uno
              icon: new Icon(Icons.account_balance_wallet_sharp),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return cuentaCosultaDos(      
                  );
                }));
              },
            ),
            IconButton(
              //Cuenta
              icon: new Icon(Icons.note),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return cuenta(      
                  );
                }));
              },
            ),
            //Comandas
             IconButton(
              icon: new Icon(Icons.book),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return comandas(      
                  );
                }));
              },
            ),
            //Reservacion
             IconButton(
              icon: new Icon(Icons.menu_book_sharp),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return reservacion(      
                  );
                }));
              },
            ),
             IconButton(
              icon: new Icon(Icons.logout),
              onPressed: () {
               cerrar_sesion();
              },
            ),
          ],
        ),
    );
  } 
   Future<void> cerrar_sesion() async {
    //prefs.remove("counter");
    SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.remove('id_usuario');
      await prefs.remove('nombre');
      await prefs.remove('contrasenia');
      await prefs.remove('rol');
      await prefs.remove('id_empleado');
      Navigator.push(
          context, MaterialPageRoute(builder: (_) => login())
      );
  } 
  var urlPost = "https://apis.pegasoirapuato.com/public/api/v1/reservacion";

  void reservacion_post(int mesa_id, int empleado_id, String cliente, String observacion, String fecha, int id_horario, String num_personas) async {
    try{

      final response = await post(Uri.parse(urlPost), body:{        
        "mesa_id":mesa_id.toString(),
        "empleado_id": empleado_id.toString(),
        "cliente": cliente,
        "observacion": observacion,
        "fecha": fecha,
        "hora": id_horario.toString(),
        "num_personas": num_personas,
      });
      
      if(response.statusCode==200){
        var body=response.body;
      var usuarios=jsonDecode(body);
        print(usuarios);
      }if(response.statusCode==201){
        var body=response.body;
      var usuarios=jsonDecode(body);
        print(usuarios);
        _showAlertSuccess(this.context);
      }else{
        _showAlertError(this.context,);
      }
    }catch(e){
      print(e);
    }
}
  void _showAlertError(BuildContext context) {
     showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('Error! '),
          content: const Text('Realice de nuevo esta acción'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return reservacion(      
                  );})),
              child: const Text('OK'),
            ),
          ],
        ));
    }
      void _showAlertSuccess(BuildContext context) {
        showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('Realizado! '),
          content: const Text('Se realizo esta acción'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return cuenta(      
                  );})),
              child: const Text('OK'),
            ),
          ],
        ));
    }

    void traer_id() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      id_empleado = (prefs.getInt('id_empleado'))!;
    });
  }
}

