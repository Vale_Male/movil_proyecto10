import 'package:comandas/CuentaConsultasDos.dart';
import 'package:comandas/comandasRegistro.dart';
import 'package:comandas/cuentaDos.dart';
import 'package:comandas/reservacionRegistro.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:comandas/comandasDosRegistro.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'CuentaUno.dart';
import 'login.dart';

//import 'package:flutter_application_introduction/ejemplo5.dart';

class cuentaDos  extends StatefulWidget {
  final Comandas comanda;

  cuentaDos(this.comanda);
  @override
  _cuentaDosState createState() => _cuentaDosState(this.comanda);
}
//Definir metodos de pagos de
  var metodos_pago = ['Efectivo','Tarjeta crédito','Tarjeta debito'];

//Traer comandas
  final String urlComandasPlatillos = "https://apis.pegasoirapuato.com/public/api/v1/comandasPlatillos/1?id=";
  final String urlPlatillos = "https://apis.pegasoirapuato.com/public/api/v1/horarios/1?id_Platillo=";
  //Creacion de color verde-agua
  class HexColor extends Color {
    
    static int _getColorFromHex(String hexColor) {
      hexColor = hexColor.toUpperCase().replaceAll("#", "");
      if (hexColor.length == 6) {
        hexColor = "FF" + hexColor;
      }
      return int.parse(hexColor, radix: 16);
    }

    HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
  }
  Color color = HexColor("#86d9ab");
enum SingingCharacter { oro, plata }
int id_emple=1;


class _cuentaDosState extends State<cuentaDos> {
    var selected_metod= 'Efectivo';

final Comandas comanda;
_cuentaDosState(Comandas this.comanda);
  List<ComandasPlatillos> comandasListaPlatillo = [];
  List<Platillos> platillosLista = [];
  List<Platillos> platillosList = [];

//platillosLista
 Future<String> traerComandas() async {
    var res = await http
        .get(Uri.parse(urlComandasPlatillos+comanda.id.toString()), headers: {"Accept": "application/json"});
    var resBody1 = json.decode(res.body);
    var resBody = resBody1['data'];
        List<ComandasPlatillos> comandasListaPlatillos = [];

    for(var i = 0; i < resBody.length; i++){
      ComandasPlatillos reser = new ComandasPlatillos();
      reser.id =resBody[i]['id'];
      reser.cantidad =int.parse(resBody[i]['cantidad']);
      reser.platilloId =int.parse(resBody[i]['platillo_id']);
      reser.precio =resBody[i]['precio'].toString();
      traerPlatillo(reser.platilloId,reser.cantidad);
      comandasListaPlatillos.add(reser);

    }
    setState(() {
      comandasListaPlatillo = comandasListaPlatillos;
    });
    return "Sucess";
  }
 Future<String> traerPlatillo(int platilloId, int cantidad) async {
    var res = await http
        .get(Uri.parse(urlPlatillos+platilloId.toString()), headers: {"Accept": "application/json"});
    var resBody1 = json.decode(res.body);
    var resBody = resBody1['data'];
    print(resBody );
/*
  late String foto;
  late int cantidad;*/
    for(var i = 0; i < resBody.length; i++){
      Platillos plat = new Platillos();
      plat.id =resBody[i]['id'];
      plat.nombre =resBody[i]['nombre'];
      plat.precio =resBody[i]['precio'].toString();
      plat.foto =resBody[i]['foto'];
      plat.cantidad =cantidad;
      platillosList.add(plat);

    }
    setState(() {
      platillosLista = platillosList;
    });
    return "Sucess";
  }
@override
  void initState() {
    super.initState();
    this.traerComandas();
    cantidad=0.0;
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
  statusBarColor: Colors.transparent,
));
    String rutaImagen="assets/img/logo.png";
    return  Scaffold(
      backgroundColor: Colors.white,

     body: Column(
        children: [
          Container(
            color: Colors.green[50],
            child: ListTile(
              title: Text(
                'Realizar pago:',
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
          ),
          Expanded(
             child: new ListView(
              children: platillosLista.map(_buildItem).toList(),
            )  
          ), ListTile(
                         subtitle: Text(
                        'Metódo de pago',
                        style: TextStyle(
                          fontSize: 20,
                          color: color,
                        ),
                      ),
                      
                    leading: Icon(
                      Icons.payment_outlined,
                      color: color,
                    ),
                  ),DropdownButton<String>(
                      items: metodos_pago.map((String dropDownStringItem){
                        return DropdownMenuItem<String>(
                          value:dropDownStringItem,
                          child:Text(dropDownStringItem),
                        ); 
                      }).toList(),
                      onChanged: (String ? value ){
                        setState(() {
                            this.selected_metod = value!;
                            var color;
                            int posi=0;
                            // ignore: unused_label
                        });
                      },
                      value:this.selected_metod,
                    ),
         Padding(
                padding: const EdgeInsets.only(
                   top: 25, bottom: 50),
                  child: Center(
                child: Text(
                  'Total a pagar:'+comanda.precio.toString(),
                  //textDirection: TextDirection.ltr,
                  style: TextStyle(
                    fontSize: 32,
                    color: color,
                  ),
                    
              ),
              ),
            ),
          Container(
            child: new Column(
              
              children: <Widget>[
                    SizedBox(height: 10),

                FlatButton(
                  onPressed: () {
                    insertar_pago(this.selected_metod);
                  },
                  child: Text(
                    'Aceptar',
                    style: TextStyle(color: Colors.black, fontSize: 12),
                  ),color: color,
                ),
                    SizedBox(height: 10),

              ]
            ) 
          ),
        ]
      ), 
        appBar: AppBar(
       backgroundColor: color,
          title: Text('Riquisimo!'),
          actions: <Widget>[
            IconButton(
              //Cuenta Uno
              icon: new Icon(Icons.account_balance_wallet_sharp),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return cuentaCosultaDos(      
                  );
                }));
              },
            ),
            IconButton(
              //Cuenta
              icon: new Icon(Icons.note),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return cuenta(      
                  );
                }));
              },
            ),
            //Comandas
             IconButton(
              icon: new Icon(Icons.book),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return comandas(      
                  );
                }));
              },
            ),
            //Reservacion
             IconButton(
              icon: new Icon(Icons.menu_book_sharp),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return reservacion(      
                  );
                }));
              },
            ),
             IconButton(
              icon: new Icon(Icons.logout),
              onPressed: () {
               cerrar_sesion();
              },
            ),
          ],
        )
    
      
    );

  }
  Future<void> cerrar_sesion() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.remove('id_usuario');
      await prefs.remove('nombre');
      await prefs.remove('contrasenia');
      await prefs.remove('rol');
      await prefs.remove('id_empleado');
      Navigator.push(
          context, MaterialPageRoute(builder: (_) => login())
      );
  }
  var urlPago= "https://apis.pegasoirapuato.com/public/api/v1/pagos";

  void insertar_pago(String selected_metod) async {
    var tipo_pago=selected_metod;
    var precio_total=comanda.precio.toString();
    var id_comand=comanda.id.toString();
    /*'tipo_pago' => 'required',
            'precio_total' => 'required',
            'comanda_id' => 'required'*/
    try{
      final response = await post(Uri.parse(urlPago), body:{
        "tipo_pago":tipo_pago,
        "precio_total":  precio_total,
        "comanda_id": id_comand
      });
      
      if(response.statusCode==201){
          _showAlertSuccess(this.context,);
      }else{
        _showAlertError(this.context,);
      }
    }catch(e){
      print(e);
    }

   /* Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return cuentaDos( comanda     
                  );
                }));*/
  }
  void _showAlertError(BuildContext context) {
       showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('Error! '),
          content: const Text('Realice de nuevo esta acción'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return reservacion(      
                  );})),
              child: const Text('OK'),
            ),
          ],
        ));
    }
  void _showAlertSuccess(BuildContext context) {
      showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('Realizado! '),
          content: const Text('Se realizo esta acción'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return reservacion(      
                  );})),
              child: const Text('OK'),
            ),
          ],
        ));
    }
  Widget _buildItem(Platillos platillos) {
  return new ListTile(
      title: new Text(platillos.nombre),
      subtitle: new Text('Precio: ${platillos.precio}, Cantidad:${platillos.cantidad}'),
      leading: new Image.network(platillos.foto,width: 50,
                                        height: 50,
                                        fit:BoxFit.fill),
  );
}
}

class ComandasPlatillos {
  late int id;
  late String precio;
  late int platilloId;
  late int cantidad;
}
