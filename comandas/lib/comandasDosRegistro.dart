import 'dart:convert';

import 'package:comandas/CuentaConsultasDos.dart';
import 'package:comandas/reservacionRegistro.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:comandas/login.dart';
import 'package:comandas/comandasRegistro.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'CuentaUno.dart';

//import 'package:flutter_application_introduction/ejemplo5.dart';

class comandasDos  extends StatefulWidget {
  comandasDos(List<Platillos> platillosList);



  @override
  _comandasDosState createState() => _comandasDosState(platillosList);
}
 //Creacion de color verde-agua
  class HexColor extends Color {
    
    static int _getColorFromHex(String hexColor) {
      hexColor = hexColor.toUpperCase().replaceAll("#", "");
      if (hexColor.length == 6) {
        hexColor = "FF" + hexColor;
      }
      return int.parse(hexColor, radix: 16);
    }

    HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
  }
  Color color1 = HexColor("#86d9ab");
enum SingingCharacter { oro, plata }
int id_empleado=0;
class _comandasDosState extends State<comandasDos> {
  
final valorOhm = TextEditingController();
List<Platillos> platilloListss = [];
double precioTotal=0.0;
double precioTotalDos=0.0;
_comandasDosState(List<Platillos> platillosList);
Future<String> traerPlatillos() async {
    List<Platillos> platilloListssDos = [];
    double precioT=0.0;
    for(var i = 0; i < platillosList.length; i++){
      if(platillosList[i].cantidad>0){
        int cant=platillosList[i].cantidad;
        precioT=double.parse(platillosList[i].precio)*cant;
        platilloListssDos.add(platillosList[i]);
        precioTotalDos=precioT+precioTotalDos;
      }
    }
    setState(() {
      platilloListss = platilloListssDos;
      precioTotal = precioTotalDos;
    });
    return "Sucess";
  }
@override
  void initState() {
    super.initState();
    this.traerPlatillos();
    this.traer_id();
    cantidad=0.0;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
  statusBarColor: Colors.transparent,
));
    String rutaImagen="assets/img/logo.png";

    return  Scaffold(
      backgroundColor: Colors.white,

        body: Column(
        children: [
          Container(
            color: Colors.green[50],
            child: ListTile(
              title: Text(
                'Verificar comanda:',
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
          ),
          Expanded(
             child: new ListView(
              children: platilloListss.map(_buildItem).toList(),
            )  
          ),
          Container(
            color: Colors.green[50],
            child: new Row(
              children: <Widget>[
                SizedBox(height: 50,width:50),
                new  Text(
                  'Total a pagar:',
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                new  Text(
                  precioTotal.toString(),
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),                
                SizedBox(height: 50,width:50),
                FlatButton(
                  onPressed: () {
                    insertar_comanda();
                  },
                  child: Text(
                    'Aceptar',
                    style: TextStyle(color: Colors.black, fontSize: 12),
                  ),color: color1,
                )
              ]
            ) 
          ),
        ]
      ),
    
    appBar: AppBar(
       backgroundColor: color1,
          title: Text('Riquisimo!'),
          actions: <Widget>[
            IconButton(
              //Cuenta Uno
              icon: new Icon(Icons.account_balance_wallet_sharp),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return cuentaCosultaDos(      
                  );
                }));
              },
            ),
            IconButton(
              //Cuenta
              icon: new Icon(Icons.note),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return cuenta(      
                  );
                }));
              },
            ),
            //Comandas
             IconButton(
              icon: new Icon(Icons.book),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return comandas(      
                  );
                }));
              },
            ),
            //Reservacion
             IconButton(
              icon: new Icon(Icons.menu_book_sharp),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return reservacion(      
                  );
                }));
              },
            ),
             IconButton(
              icon: new Icon(Icons.logout),
              onPressed: () {
               cerrar_sesion();
              },
            ),
          ],
        )
    
    );
  }
  Future<void> cerrar_sesion() async {
    //prefs.remove("counter");
    SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.remove('id_usuario');
      await prefs.remove('nombre');
      await prefs.remove('contrasenia');
      await prefs.remove('rol');
      await prefs.remove('id_empleado');
      Navigator.push(
          context, MaterialPageRoute(builder: (_) => login())
      );
  } 
Widget _buildItem(Platillos platillos) {
  return new ListTile(
      title: new Text(platillos.nombre),
      subtitle: new Text('Precio: ${platillos.precio}, Cantidad:${platillos.cantidad}'),
      leading: new Image.network(platillos.foto,width: 50,
                                        height: 50,
                                        fit:BoxFit.fill),
  );
}
  var urlTr= "https://apis.pegasoirapuato.com/public/api/v1/comandas";

  void insertar_comanda() async {

    try{
      final response = await post(Uri.parse(urlTr), body:{

        "precio":precioTotal.toString(),
        "empleado_id": id_empleado.toString()
      });      
      if(response.statusCode==201){
        var body=response.body;
      var body_message=jsonDecode(body);
      print(body_message['message']);
      var idCom=body_message['message'];
      int cantiProb=platilloListss.length-1;
      for(var i=0;i<platilloListss.length;i++){
        var booleana=false;
        if(i==cantiProb){
          booleana=true;
        }
        insertar_comanda_platillo(platilloListss[i],idCom.toString(),booleana);
      }
      //var usuario_status=usuarios['usuario'];
      //var role=usuario_status['rol'];
        
      }else{
        _showAlertError(this.context,);
      }
    }catch(e){
      print(e);
    }

}
//comandasPlatillos
  var urlPlaCom= "https://apis.pegasoirapuato.com/public/api/v1/comandasPlatillos";

 void insertar_comanda_platillo(Platillos platillo, String id_comanda,var booleana) async {
    var id_comand=id_comanda.toString();

    try{
      final response = await post(Uri.parse(urlPlaCom), body:{
        "comandas_id":id_comand.toString(),
        "platillo_id":  platillo.id.toString(),
        "cantidad": platillo.cantidad.toString()
      });
      
      if(response.statusCode==201){
        if(booleana==true){
          _showAlertSuccess(this.context,);
        }
      }else{
        _showAlertError(this.context,);
      }
    }catch(e){
      print(e);
    }

}
void _showAlertSuccess(BuildContext context) {
       showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('Realizado! '),
          content: const Text('Se realizo esta acción'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return comandas(      
                  );})),
              child: const Text('OK'),
            ),
          ],
        ));
    }
  void _showAlertError(BuildContext context) {
      showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('Error! '),
          content: const Text('Realice de nuevo esta acción'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return comandas(      
                  );})),
              child: const Text('OK'),
            ),
          ],
        ));
    }


void traer_id() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      id_empleado = (prefs.getInt('id_empleado'))!;
    });
  }
}



class _selectedLocation {
}