import 'package:comandas/CuentaConsultasDos.dart';
import 'package:comandas/CuentaUno.dart';
import 'package:comandas/reservacionRegistro.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:comandas/comandasDosRegistro.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import 'login.dart';

//import 'package:flutter_application_introduction/ejemplo5.dart';

class comandas  extends StatefulWidget {
  @override
  _comandasState createState() => _comandasState();
}
//Traer reservacions
final String urlDos = "https://apis.pegasoirapuato.com/public/api/v1/posts";
  List<Platillos> platillosList = [];
  List<PlatillosComandas> platillosComandasList = [];
   double cantidad=0.0;
  //Creacion de color verde-agua
  class HexColor extends Color {
    
    static int _getColorFromHex(String hexColor) {
      hexColor = hexColor.toUpperCase().replaceAll("#", "");
      if (hexColor.length == 6) {
        hexColor = "FF" + hexColor;
      }
      return int.parse(hexColor, radix: 16);
    }

    HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
  }
  Color color = HexColor("#86d9ab");
enum SingingCharacter { oro, plata }



class _comandasState extends State<comandas> {

 Future<String> traerPlatillos() async {
    var res = await http
        .get(Uri.parse(urlDos), headers: {"Accept": "application/json"});
    var resBody1 = json.decode(res.body);
    var resBody = resBody1['data'];
    List<Platillos> platilloListss = [];
    // String fecha,String hora,int num_personas,int id) {

  print(resBody);
   for(var i = 0; i < resBody.length; i++){

      Platillos reser = new Platillos();
            
            reser.nombre =resBody[i]['nombre'];
            reser.precio =resBody[i]['precio'];
            reser.foto =resBody[i]['foto'];
            reser.estatus =resBody[i]['estatus'];
            reser.id =resBody[i]['id'];
            reser.cantidad =0;
      platilloListss.add(reser);
    }
    setState(() {
      platillosList = platilloListss;
    });
    return "Sucess";
  }

@override
  void initState() {
    super.initState();
    this.traerPlatillos();
    cantidad=0.0;
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
  statusBarColor: Colors.transparent,
));
    String rutaImagen="assets/img/logo.png";
    return  Scaffold(
      backgroundColor: Colors.white,

     body: Center (
       child:SingleChildScrollView(
       
        child: Column(
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.only(
                   top: 25, bottom: 50),
                  child: Center(
                child: Text(
                  'Comandas',
                  textDirection: TextDirection.ltr,
                  style: TextStyle(
                    fontSize: 32,
                    color: color,
                  ),
                    
              ),
              ),
            ),
             Row(
                children:[
                   Padding(
                     padding: EdgeInsets.only(right: 50),
                  ),
                  new SizedBox(
                      width: 60.0,
                      height: 20.0,
                      child: Text(
                          r'Total: $',
                          textDirection: TextDirection.ltr,
                          style: TextStyle(
                            fontSize: 20,
                            color: color,
                          ),
                            
                      ),
                  ),
                  new SizedBox(
                      width: 30.0,
                      height: 20.0,
                  ),
                  new SizedBox(
                      width: 60.0,
                      height: 20.0,
                      child: new  Text(
                          cantidad.toString(),
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.black,
                          ),
                     )
                    ),
                ]

            ),
            SizedBox(
            child: Card(
              child: Column(
                children: [                
                   Container(
                child:  GridView.builder(
                  physics: ScrollPhysics(),
                  shrinkWrap: true,
                    itemCount: platillosList.length,
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      childAspectRatio: 0.5,
                    ),
                    itemBuilder: (context, index) {;
                      return Container(
                        
                        width:500,
                        height:600,
                        margin: EdgeInsets.all(5),
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0x000005cc),
                                  blurRadius: 30,
                                  offset: Offset(10, 10))
                            ]),
                        child: Column(
                          
                          children: <Widget>[
                            Image.network(platillosList[index].foto+'?raw=true',width: 100,
                                        height: 100,
                                        fit:BoxFit.fill),
                            Text(
                              platillosList[index].nombre,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 20),
                              child: Text(
                                r"$"+ platillosList[index].precio.toString(),
                                style: TextStyle(fontSize: 16),
                              ),
                            ),Padding(
                              padding: EdgeInsets.only(top: 10),
                              child: Text(
                                 //controller:txt,
                                 platillosList[index].cantidad.toString(),
                                style: TextStyle(fontSize: 16),
                              ),
                            ),
                               
                            Row(
                              children:[
                                new SizedBox(
                                  width: 40.0,
                                  height: 20.0,
                                  child: new RaisedButton(
                                    onPressed: () {
                                      setState(() {
                                        platillosList[index].cantidad=platillosList[index].cantidad+1;
                                        sumarCantidad(platillosList[index].precio);
                                       });
                                    },
                                    child: new Text('+',style: TextStyle(color: Colors.white),),
                                    color: Colors.green,
                                  ),
                                ),
                                new SizedBox(
                                  width: 30.0,
                                  height: 20.0,
                                ),
                                new SizedBox(
                                  width: 40.0,
                                  height: 20.0,
                                  child: new RaisedButton(
                                    onPressed: () {
                                       setState(() {
                                         if(platillosList[index].cantidad==0){
                                           platillosList[index].cantidad=platillosList[index].cantidad;
                                         }else{
                                          platillosList[index].cantidad=platillosList[index].cantidad-1;
                                          restarCantidad(platillosList[index].precio);
                                         }
                                       });
                                    },
                                    child: new Text('-',style: TextStyle(color: Colors.white),),
                                    color: Colors.red,
                                  ),
                                ),
                              ]

                            )
                         
                            ],
                          /*  RaisedButton.icon(onPressed: () {
                              print("This");

                            }, icon: new Icon(Icons.add ), label: Text('')),
                         RaisedButton.icon(onPressed: () {
                              print("This");
                              
                            }, icon: new Icon(Icons.more), label: Text('-'))
                          ],*/
                        ),
                      );
                    }),
              ),
                  Divider(),
                  
                Container(
                  
                  height: 40,
                  decoration: BoxDecoration(
                      color: color, borderRadius: BorderRadius.circular(20)),
                  child: FlatButton(
                    onPressed: () {
                       comandasDosSigueinte(this.context);
                      
                    },
                    child: Text(
                      'Siguiente',
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    ),
                  ),
                ),


                ],
              ),
            ),
            
          ),
           
          ],
        ),
      
      )
      ),
       appBar: AppBar(
       backgroundColor: color,
          title: Text('Riquisimo!'),
          actions: <Widget>[
            IconButton(
              //Cuenta Uno
              icon: new Icon(Icons.account_balance_wallet_sharp),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return cuentaCosultaDos(      
                  );
                }));
              },
            ),
            IconButton(
              //Cuenta
              icon: new Icon(Icons.note),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return cuenta(      
                  );
                }));
              },
            ),
            //Comandas
             IconButton(
              icon: new Icon(Icons.book),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return comandas(      
                  );
                }));
              },
            ),
            //Reservacion
             IconButton(
              icon: new Icon(Icons.menu_book_sharp),
              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                  return reservacion(      
                  );
                }));
              },
            ),
             IconButton(
              icon: new Icon(Icons.logout),
              onPressed: () {
               cerrar_sesion();
              },
            ),
          ],
        )
    
    );

  }
  Future<void> cerrar_sesion() async {
    //prefs.remove("counter");
    SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.remove('id_usuario');
      await prefs.remove('nombre');
      await prefs.remove('contrasenia');
      await prefs.remove('rol');
      await prefs.remove('id_empleado');
      Navigator.push(
          context, MaterialPageRoute(builder: (_) => login())
      );
  } 
  void sumarCantidad(String precio) {
    double total=double.parse(precio);
    setState(() {
      cantidad = cantidad+total;
    });
  }
  void restarCantidad(String precio) {
    double total=double.parse(precio);
    double totalDos =0.0;
    if(cantidad==0){
      totalDos=0;
    }else{
      totalDos = cantidad-total;
    }
  setState(() {
      cantidad = totalDos;
    });
  }
}

void comandasDosSigueinte(BuildContext context) {

  Navigator.push(context, MaterialPageRoute(builder: (_) => comandasDos(platillosList)));
}

class PlatillosComandas {
  late int id;
  late int cantidad;
}

class Platillos {
  late int id;
  late String nombre;
  late String precio;
  late String foto;
  late String estatus;
  late int cantidad;
}
